package de.wsc.costs;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void calculate() {
        assertEquals(new BigDecimal("1.00"), Calculator.calculate("1"));
        assertEquals(new BigDecimal("1.20"), Calculator.calculate("1.2"));
        assertEquals(new BigDecimal("1.20"), Calculator.calculate("0.2 + 1"));
        assertEquals(new BigDecimal("2.20"), Calculator.calculate("0.2 + 0.2*10"));
        assertEquals(new BigDecimal("4.00"), Calculator.calculate("(0.2 + 0.2)*10"));
        assertEquals(new BigDecimal("1.33"), Calculator.calculate("1+1/3"));
        assertEquals(new BigDecimal("9.00"), Calculator.calculate("10*.9"));
        assertEquals(new BigDecimal("9.00"), Calculator.calculate("2*4=+1"));
        assertEquals(new BigDecimal("12.00"), Calculator.calculate("2+4=*2"));
        assertNull(Calculator.calculate(null));
    }

    @Test
    void calculateGerman() {
        assertEquals(new BigDecimal("1.00"), Calculator.calculateGerman("1"));
        assertEquals(new BigDecimal("1.20"), Calculator.calculateGerman("1,2"));
        assertEquals(new BigDecimal("1.20"), Calculator.calculateGerman("0,2 + 1"));
        assertEquals(new BigDecimal("2.20"), Calculator.calculateGerman("0,2 + 0,2*10"));
        assertEquals(new BigDecimal("4.00"), Calculator.calculateGerman("(0,2 + 0,2)*10"));
        assertEquals(new BigDecimal("1.33"), Calculator.calculateGerman("1+1/3"));
        assertEquals(new BigDecimal("9.00"), Calculator.calculateGerman("10*,9"));
        assertEquals(new BigDecimal("9.00"), Calculator.calculateGerman("2*4=+1"));
        assertEquals(new BigDecimal("12.00"), Calculator.calculateGerman("2+4=*2"));
        assertEquals(new BigDecimal("23.05"), Calculator.calculateGerman("1,68+,18+,59+1,83+2,61+,87+3,48+1,22+1,92+3,35+1,06+2,48+1,78"));
        assertNull(Calculator.calculate(null));
    }
}