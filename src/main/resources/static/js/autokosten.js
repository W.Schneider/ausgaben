window.select = function (rowElement) {
    document.getElementById("betrag").value=rowElement.querySelector("[data-betrag]").dataset.betrag;
    document.getElementById("datum").value=rowElement.querySelector("time").dateTime;
    document.getElementById("tageskilometer").value=rowElement.querySelector("[data-tageskilometer]").dataset.tageskilometer;
    document.getElementById("gesamtkilometer").value=rowElement.querySelector("[data-gesamtkilometer]").dataset.gesamtkilometer;
    document.getElementById("liter").value=rowElement.querySelector("[data-liter]").dataset.liter;
    document.getElementById("description").value=rowElement.querySelector("[data-description]").dataset.description;
    document.getElementById("id").value = rowElement.dataset.id;

    document.querySelectorAll(".table-row-group .table-row").forEach(e => e.classList.remove("!bg-red-100"));
    rowElement.classList.add("!bg-red-100");
}
