function selectAusgabe(row) {
    deselectRows();
    const radioButton = row.querySelector("input[type='radio']")
    radioButton.checked = true;
    radioButton.focus();
    document.getElementById("costid").value = row.dataset['id'];
    document.getElementById("date").value = row.dataset['date'];
    document.getElementById("name").value = row.dataset['name'];
    document.getElementById("category").value = row.dataset['category'];
    document.getElementById("formula").value = row.dataset['formula'];
    document.getElementById("receipt").value = row.dataset['receipt'];
}

function deselectRows() {
    const rows = document.querySelectorAll(".table-row input[type='radio']");
    for (let i = 0; i < rows.length; i++) {
        rows[i].checked = false;
    }
}

function resetForm() {
    document.getElementById("costid").value = "";

    const date = new Date();
    document.getElementById("date").value = date.toISOString().substring(0, 10);
    document.getElementById("name").value = "";
    document.getElementById("category").value = "";
    document.getElementById("formula").value = "";
    document.getElementById("receipt").value = "0";

    deselectRows();
    document.getElementById("date").focus();
}

function submitOnEnter(event) {
    if (event.code === "Enter" && !event.shiftKey) {
        event.preventDefault(); // Prevents the addition of a new line in the text field (not needed in a lot of cases)
        const target = event.target;
        if (isInputValid()) {
            target.form.requestSubmit();
        }
    }
}

function selectKeyUp(event) {
    if (event.key === "Enter") {
        document.getElementById("date").focus();
    }
    if (event.key === "Delete") {
        document.forms['delete'].elements['id'].value = event.target.dataset['id'];
        document.forms['delete'].requestSubmit();
    }
}

function showReceiptField() {
    document.getElementById('receiptField').classList.remove('hidden');
    document.getElementById('receipt').type = 'number';
}

function isInputValid() {
    let ok = true;
    ok &= (document.getElementById("date").value.length > 0);
    ok &= (document.getElementById("name").value.length > 0);
    ok &= (document.getElementById("category").value.length > 0);
    ok &= (document.getElementById("formula").value.length > 0);
    return ok;
}

function wire() {
    addEventListener("#resetButton", "click", resetForm);
    addEventListener("[name='selectButton']", "keyup", selectKeyUp);
    addEventListener("#formula", "keypress", submitOnEnter);
    addEventListener("#newReceiptButton", "click", showReceiptField);
}

function addEventListener(selector, event, method) {
    const elements = document.querySelectorAll(selector);
    if (elements) {
        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener(event, method);
        }
    }
}

export {selectAusgabe, wire}
