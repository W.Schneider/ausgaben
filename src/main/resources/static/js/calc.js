(function calc() {
    const messageElement = document.getElementById("message");
    let message = "";
    function wire() {
        const buttons = document.querySelectorAll("button");
        for (let i = 0; i < buttons.length; i++) {
            const button = buttons[i];
            button.addEventListener("click", function(evnt) {
                message.innerText = evnt.target.innerText;
            })
        }
        document.addEventListener("keydown", function(evnt) {
            if (isValidCode(evnt.key)) {
                message += evnt.key + " ";
                messageElement.innerText = message;
            }
        })
    }

    const validCodes = ["1","2","3","4","5","6","7","8","9","0", "/", "+", "-", "*", "=", ".", "c", "Enter", "Escape", "Backspace"]

    function isValidCode(code) {
        return validCodes.includes(code);
    }

    wire();

    messageElement.innerText =  eval("1.8+8/2+2*3");
})();