package de.wsc.costs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {

    private final AutokostenController autokostenController;
    private final CategoryController categoryController;
    private final CostController costController;
    private final NamesController namesController;
    private final ReportingController reportingController;

    @Value("${costs.stage}")
    private String stage;

    public AppController(AutokostenController autokostenController, CategoryController categoryController,
                         CostController costController, NamesController namesController, ReportingController reportingController) {
        this.autokostenController = autokostenController;
        this.categoryController = categoryController;
        this.costController = costController;
        this.namesController = namesController;
        this.reportingController = reportingController;
    }

    @RequestMapping({"/{page}", "/", "/"})
    public String app(Model model, OldValues oldValues, @PathVariable(required = false) String page, @PathVariable(required = false) String menu) {
        model.addAttribute("stage", stage);
        model.addAttribute("menu", menu);
        if (page == null) {
            model.addAttribute("page", "costs");
            costController.fillCosts(model, oldValues);
        } else {
            model.addAttribute("page", page);
            switch (page) {
                case "costs" -> costController.fillCosts(model, oldValues);
                case "auto" -> autokostenController.fillAutokosten(model);
                case "categories" -> categoryController.fillCategories(model);
                case "names" -> namesController.fillNames(model);
                case "reporting" -> reportingController.fillReporting(model);
                case "spielwiese" -> {
                    return "spielwiese";
                }
            }
        }
        return "app";
    }
}
