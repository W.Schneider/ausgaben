package de.wsc.costs;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity()
public class Cost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String name;
    private BigDecimal amount;
    private String category;
    private String formula;
    private Integer receipt;

    public Cost(LocalDate date, String name, BigDecimal amount, String category, String formula, int receipt) {
        this.date = date;
        this.name = name;
        this.amount = amount;
        this.category = category;
        this.formula = formula;
        this.receipt = receipt;
    }

    public Cost() {}

    public LocalDate getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCategory() {
        return category;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Integer getReceipt() {
        return receipt;
    }

    public void setReceipt(Integer receipt) {
        this.receipt = receipt;
    }

    @Override
    public String toString() {
        return "Cost{" +
                "id=" + id +
                ", date=" + date +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", category='" + category + '\'' +
                ", formula='" + formula + '\'' +
                ", receipt='" + receipt + '\'' +
                '}';
    }
}

