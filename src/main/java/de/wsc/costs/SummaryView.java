package de.wsc.costs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SummaryView {
    private BigDecimal total = BigDecimal.ZERO;
    private final int year;
    private final List<Summary> categorySummaries = new ArrayList<>();

    public SummaryView(int year) {
        this.year = year;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public int getYear() {
        return year;
    }

    public List<Summary> getCategorySummaries() {
        return new ArrayList<>(categorySummaries);
    }

    public void addTotal(BigDecimal amount) {
        total = total.add(amount);
    }

    public void addSummary(Summary summary) {
        categorySummaries.add(summary);
    }

    public static List<SummaryView> getSummaryViews(List<Summary> summaries) {
        List<SummaryView> views = new ArrayList<>();
        if (summaries == null || summaries.isEmpty()) {
            return views;
        }

        SummaryView view = new SummaryView(summaries.get(0).getYear());

        for (Summary summary : summaries) {
            if (view.year != summary.getYear()) {
                views.add(view);
                view = new SummaryView(summary.getYear());
            }
            view.addTotal(summary.getAmount());
            view.addSummary(summary);
        }
        views.add(view);
        return views;
    }
}
