package de.wsc.costs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CategoryController {

    private final CostRepository costRepository;

    @Value("${costs.stage}")
    private String stage;

    public CategoryController(CostRepository costRepository) {
        this.costRepository = costRepository;
    }

    @GetMapping(value = "/fragments/categories")
    public String kategorien(Model model) {
        fillCategories(model);
        return "fragments/kategorien";
    }

    @PostMapping(value = "/fragments/categories")
    public String kategorie(Model model, String oldValue,  String newValue) {
        costRepository.updateCategoryName(oldValue, newValue);
        return kategorien(model);
    }

    public void fillCategories(Model model) {
        model.addAttribute("categories", costRepository.findDistinctCategories());
        model.addAttribute("stage", stage);
    }
}
