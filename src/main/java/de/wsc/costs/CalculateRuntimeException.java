package de.wsc.costs;


public class CalculateRuntimeException extends RuntimeException {
    public CalculateRuntimeException(Throwable throwable) {
        super(throwable);
    }
}
