package de.wsc.costs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@SpringBootApplication
public class CostApplication implements WebMvcConfigurer {



	public static void main(String[] args) {
		SpringApplication.run(CostApplication.class, args);
	}

    /*
    @Bean
    public CommandLineRunner demo(InitializeEmptyFormula initializeEmptyFormula) {
        return args -> {
        };
    }
     */
}
