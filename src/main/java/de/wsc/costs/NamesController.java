package de.wsc.costs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NamesController {

    private final CostRepository costRepository;

    @Value("${costs.stage}")
    private String stage;

    public NamesController(CostRepository costRepository) {
        this.costRepository = costRepository;
    }

    @GetMapping(value = "/fragments/names")
    public String names(Model model) {
        fillNames(model);
        return "fragments/namen";
    }

    @PostMapping(value = "/fragments/names")
    public String name(Model model, String oldValue, String newValue) {
        costRepository.updateName(oldValue, newValue);
        return names(model);
    }

    public void fillNames(Model model) {
        model.addAttribute("names", costRepository.findDistinctNames());
        model.addAttribute("stage", stage);
    }
}
