package de.wsc.costs;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface CostRepository extends CrudRepository<Cost, Long> {

    List<Cost> findAllByOrderByDateDescIdDescNameAsc();
    List<Cost> findFirst100ByOrderByDateDescReceiptDescIdDesc();
    List<Cost> findFirst100ByDateLessThanEqualOrderByDateDescReceiptDescIdDesc(LocalDate date);
    List<Cost> findFirst500ByOrderByDateDescIdDesc();

    List<Cost> findByCategory(String category);
    List<Cost> findAllByCategoryAndDateBetweenOrderByDate(String category, LocalDate fromDate, LocalDate toDate);
    List<Cost> findAllByCategoryAndDateGreaterThanEqualOrderByDate(String category, LocalDate date);
    List<Cost> findAllByCategoryAndDateBeforeOrderByDate(String category, LocalDate date);
    List<Cost> findAllByDateBeforeOrderByDate(LocalDate date);
    List<Cost> findAllByDateGreaterThanEqualOrderByDate(LocalDate date);
    List<Cost> findAllByDateBetweenOrderByDate(LocalDate fromDate, LocalDate toDate);

    List<Cost> findFirst10ByCategoryOrderByDateDesc(String category);

    List<Cost> findFirst10ByNameOrderByDateDesc(String name);

    Cost findById(long id);


    @Query("SELECT DISTINCT a.category FROM Cost a order by a.category")
    List<String> findDistinctCategories();

    @Query("SELECT DISTINCT a.name FROM Cost a order by a.name")
    List<String> findDistinctNames();

    @Query(nativeQuery = true, value = "select c.CATEGORY as category, year(c.DATE) as year, sum(c.AMOUNT) as amount from COST c group by c.CATEGORY, year(c.DATE) order by year(c.DATE) desc, sum(c.amount) desc")
    List<Summary> findCostSummary();

    @Transactional
    @Modifying
    @Query("update Cost a set a.category = ?2 where a.category = ?1")
    int updateCategoryName(String oldName, String newName);

    @Transactional
    @Modifying
    @Query("update Cost a set a.name = ?2 where a.name = ?1")
    int updateName(String oldName, String newName);

}
