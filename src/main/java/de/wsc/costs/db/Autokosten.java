package de.wsc.costs.db;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name="autokosten")
public class Autokosten {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "beschreibung")
    private String description;
    private BigDecimal betrag = BigDecimal.ZERO;
    private LocalDate datum;
    private Long gesamtkilometer = 0L;
    private BigDecimal liter = BigDecimal.ZERO;
    private Long tageskilometer = 0L;
    @Transient
    private BigDecimal average;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String beschreibung) {
        this.description = beschreibung;
    }

    public BigDecimal getBetrag() {
        return betrag;
    }

    public void setBetrag(BigDecimal betrag) {
        this.betrag = betrag;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public Long getGesamtkilometer() {
        return gesamtkilometer;
    }

    public void setGesamtkilometer(Long gesamtkilometer) {
        this.gesamtkilometer = gesamtkilometer;
    }

    public BigDecimal getLiter() {
        return liter;
    }

    public void setLiter(BigDecimal liter) {
        this.liter = liter;
    }

    public Long getTageskilometer() {
        return tageskilometer;
    }

    public void setTageskilometer(Long tageskilometer) {
        this.tageskilometer = tageskilometer;
    }

/*
    public BigDecimal getAverage() {
        if (tageskilometer > 0) {
            return liter.multiply(BigDecimal.valueOf(100)).divide(BigDecimal.valueOf(tageskilometer), 2, RoundingMode.HALF_DOWN);
        } else {
            return BigDecimal.ZERO;
        }
    }
*/

    public BigDecimal getAverage() {
        return average;
    }

    public void setAverage(BigDecimal average) {
        this.average = average;
    }
}
