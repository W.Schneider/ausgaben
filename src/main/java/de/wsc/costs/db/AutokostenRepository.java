package de.wsc.costs.db;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AutokostenRepository extends CrudRepository<Autokosten, Long> {

    List<Autokosten> findAllByOrderByGesamtkilometerDesc();
}
