package de.wsc.costs.db.onlyonce;

import de.wsc.costs.Cost;
import de.wsc.costs.CostRepository;
import org.springframework.stereotype.Component;

@Component
public class InitializeEmptyFormula {
    private final CostRepository repository;

    public InitializeEmptyFormula(CostRepository repository) {
        this.repository = repository;
    }
    public void doInitialize() {
       for (Cost cost : repository.findAll()) {
           if (cost.getFormula() == null) {
               cost.setFormula(cost.getAmount().toString());
               repository.save(cost);
           }
       }
    }
}
