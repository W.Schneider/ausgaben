package de.wsc.costs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class ReportingController {
    private final CostRepository costRepository;

    @Value("${costs.stage}")
    private String stage;

    public ReportingController(CostRepository costRepository) {
        this.costRepository = costRepository;
    }

    @GetMapping(value = "/fragments/reporting")
    public String reporting(Model model) {
        fillReporting(model);
        return "fragments/reporting";
    }

    public void fillReporting(Model model) {
        model.addAttribute("stage", stage);
        List<Summary> summaries = costRepository.findCostSummary();
        model.addAttribute("summaryViews", SummaryView.getSummaryViews(summaries));
        model.addAttribute("categories", costRepository.findDistinctCategories());
    }

    @PostMapping(value = "/fragments/reporting")
    public String postReporting(Model model, SearchData searchData) {
        LocalDate from = searchData.getFromDate();
        LocalDate to = searchData.getToDate();
        String category = searchData.getCategory();
        if (category.isEmpty()) {
            category = null;
        }
        Iterable<Cost> costs;
        if (category != null && from != null && to != null) {
            costs = costRepository.findAllByCategoryAndDateBetweenOrderByDate(category, from, to);
            model.addAttribute("costs", costs);
        } else if (category != null && from != null) {
            costs = costRepository.findAllByCategoryAndDateGreaterThanEqualOrderByDate(category, from);
            model.addAttribute("costs", costs);
        } else if (category != null && to != null) {
            costs = costRepository.findAllByCategoryAndDateBeforeOrderByDate(category, to);
            model.addAttribute("costs", costs);
        } else if (category != null) {
            costs = costRepository.findByCategory(category);
            model.addAttribute("costs", costs);
        } else if (from != null && to != null) {
            costs = costRepository.findAllByDateBetweenOrderByDate(from, to);
            model.addAttribute("costs", costs);
        } else if (from != null) {
            costs = costRepository.findAllByDateGreaterThanEqualOrderByDate(from);
            model.addAttribute("costs", costs);
        } else if (to != null) {
            costs = costRepository.findAllByDateBeforeOrderByDate(to);
            model.addAttribute("costs", costs);
        } else {
            costs = costRepository.findAll();
            model.addAttribute("costs", costs);
        }
        model.addAttribute("total", getTotal(costs));
        return reporting(model);
    }

    private BigDecimal getTotal(Iterable<Cost> costs) {
        BigDecimal total = BigDecimal.ZERO;
        for (Cost cost : costs) {
            total = total.add(cost.getAmount());
        }
        return total;
    }
}
