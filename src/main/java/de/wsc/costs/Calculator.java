package de.wsc.costs;

import com.udojava.evalex.Expression;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    public static BigDecimal calculate(String expressionString) {
        if (expressionString == null) {
            return null;
        }

        String[] expressions = expressionString.split("=");
        BigDecimal result = null;
        for (String exp : expressions) {
            String currentExpression = exp;
            if (result != null) {
                currentExpression = result + currentExpression;
            }
            Expression expression = new Expression(currentExpression);
            result = expression.eval();
            result = result.setScale(2, RoundingMode.HALF_EVEN);
        }
        return result;
    }

    public static BigDecimal calculateGerman(String germanExpressionString) {
        String expressionString = germanExpressionString.replaceAll("\\.", "");
        expressionString = expressionString.replace(",", ".");
        return calculate(expressionString);

    }
}
