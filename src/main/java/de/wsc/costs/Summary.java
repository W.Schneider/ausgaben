package de.wsc.costs;

import java.math.BigDecimal;

public interface Summary {
    String getCategory();
    int getYear();
    BigDecimal getAmount();
}
