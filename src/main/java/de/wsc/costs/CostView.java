package de.wsc.costs;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CostView {

    private Long id;
    private LocalDate date;
    private String name;
    private BigDecimal amount;
    private String category;
    private String formula;

    private boolean zebra;
    private BigDecimal sum;
    private int receipt;

    public CostView() {}

    public CostView(Cost cost) {
        id = cost.getId();
        date = cost.getDate();
        name = cost.getName();
        amount = cost.getAmount();
        category = cost.getCategory();
        formula = cost.getFormula();
        receipt = cost.getReceipt();
    }

    public static List<CostView> getCostViews(List<Cost> costs) {
        List<CostView> costViews = new ArrayList<>();
        String name = null;
        int receipt = 0;
        LocalDate localDate = null;
        boolean zebra = false;
        BigDecimal belegSum = BigDecimal.ZERO;
        CostView topBelegCostView = null;
        for (Cost cost :costs) {
            CostView costView = new CostView(cost);
            if (cost.getName().equals(name) && cost.getDate().equals(localDate) && cost.getReceipt() == receipt) {
                belegSum = belegSum.add(cost.getAmount());
            } else {
                zebra = !zebra;
                topBelegCostView = costView;
                name = cost.getName();
                receipt = cost.getReceipt();
                localDate = cost.getDate();
                belegSum = cost.getAmount();
            }
            topBelegCostView.setSum(belegSum);
            costViews.add(costView);
            costView.setZebra(zebra);
        }

        return costViews;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCategory() {
        return category;
    }

    public boolean isZebra() {
        return zebra;
    }

    public void setZebra(boolean zebra) {
        this.zebra = zebra;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public String getFormula() {
        return formula;
    }

    public int getReceipt() {
        return receipt;
    }
}
