package de.wsc.costs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

@Controller
public class CostController {

    private static final DecimalFormat decimalFormat = new DecimalFormat("0.00");

    private final CostRepository costRepository;

    @Value("${costs.stage}")
    private String stage;

    public CostController(CostRepository costRepository) {
        this.costRepository = costRepository;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(BigDecimal.class, new BigDecimalEditor());
    }

    @GetMapping("/fragments/costs")
    public String costs(Model model, OldValues oldValues) {
        fillCosts(model, oldValues);
        return "fragments/erfassen";
    }

    public void fillCosts(Model model, OldValues oldValues) {
        List<CostView> views = CostView.getCostViews(costRepository.findFirst100ByDateLessThanEqualOrderByDateDescReceiptDescIdDesc(oldValues.getDate()));
        model.addAttribute("costs", views);
        model.addAttribute("categories", costRepository.findDistinctCategories());
        model.addAttribute("names", costRepository.findDistinctNames());
        model.addAttribute("oldValues", oldValues);
        model.addAttribute("stage", stage);
        model.addAttribute("receipt", oldValues.getReceipt());
        model.addAttribute("error", oldValues.getError());
        model.addAttribute("formula", oldValues.getFormula());
        model.addAttribute("category", oldValues.getCategory());
    }

    @GetMapping("/fragments/edit")
    public String edit(Model model, CostFilter filter) {
        return "edit";
    }

    @GetMapping("/fragments/costs/rows")
    public String costsRows(Model model, CostFilter filter) {
        model.addAttribute("costs", getCosts(filter));
        return "fragments/rows";
    }

    private List<Cost> getCosts(CostFilter filter) {
        if (filter.getCategory() != null) {
            return costRepository.findFirst10ByCategoryOrderByDateDesc(filter.getCategory());
        }
        if (filter.getName() != null) {
            return costRepository.findFirst10ByNameOrderByDateDesc(filter.getName());
        }
        return costRepository.findFirst100ByOrderByDateDescReceiptDescIdDesc();
    }

    @PostMapping("/fragments/create")
    public String create(Model model, Cost cost, OldValues oldValues) {
        try {
            cost.setAmount(Calculator.calculateGerman(cost.getFormula()));
            costRepository.save(cost);
            oldValues = new OldValues(cost.getDate(), cost.getName(), null, cost.getReceipt(), null, null, null);
        } catch (RuntimeException e) {
            oldValues.setError(e.getMessage());
        }
        return costs(model, oldValues);
    }

    @DeleteMapping("/fragments/delete")
    public String delete(Model model, OldValues oldValues) {
        Optional<Cost> optionalCost = costRepository.findById(oldValues.getId());
        if (optionalCost.isPresent()) {
            Cost cost = optionalCost.get();
            costRepository.delete(cost);
            model.addAttribute("date", cost.getDate());
        }
        fillCosts(model, oldValues);
        return "fragments/erfassen";
    }

    @DeleteMapping("/fragments/delete/{id}")
    public String deletePath(Model model, OldValues oldValues) {
        return delete(model, oldValues);
    }

    @GetMapping(value = "/fragments/download")
    public ResponseEntity<byte[]> download() { // (1) Return byte array response
        String export = exportCsv();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE); // (3) Content-Type: application/octet-stream
        httpHeaders.set(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition.attachment().filename("costs.csv").build().toString()); // (4) Content-Disposition: attachment; filename="demo-file.txt"
        return ResponseEntity.ok().headers(httpHeaders).body(export.getBytes()); // (5) Return Response
    }

    private String exportCsv() {
        StringBuilder csv = new StringBuilder();
        csv.append("Date;Name;Category;Amount\n");
        List<Cost> costs = costRepository.findAllByOrderByDateDescIdDescNameAsc();
        for (Cost cost : costs) {
            csv.append(cost.getDate()).append(';');
            csv.append(cost.getName()).append(';');
            csv.append(cost.getCategory()).append(';');
            String amount = decimalFormat.format(cost.getAmount());
            csv.append(amount).append(';');
            csv.append('\n');
        }
        return csv.toString();
    }
}
