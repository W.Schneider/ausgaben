package de.wsc.costs;

import java.time.LocalDate;

public class OldValues {
    private final LocalDate date;
    private final String name;
    private final Long id;
    private final int receipt;
    private String error;
    private final String formula;
    private final String category;

    public OldValues(LocalDate date, String name, Long id, Integer receipt, String error, String formula, String category) {
        this.date = date;
        this.name = name;
        this.id = id;
        this.receipt = receipt == null ? 0 : receipt;
        this.error = error;
        this.formula = formula;
        this.category = category;
    }

    public LocalDate getDate() {
        if (date == null) {
            return LocalDate.now();
        }
        return date;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }
    public boolean isEmpty() {
        return date == null && name == null;
    }

    public int getReceipt() {
        return receipt;
    }

    public String getError() {
        return error;
    }

    public String getFormula() {
        return formula;
    }

    public String getCategory() {
        return category;
    }

    public void setError(String error) {
        this.error = error;
    }
}
