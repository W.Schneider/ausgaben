package de.wsc.costs;

import de.wsc.costs.db.Autokosten;
import de.wsc.costs.db.AutokostenRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AutokostenController {

    private final AutokostenRepository repository;

    @Value("${costs.stage}")
    private String stage;

    public AutokostenController(AutokostenRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/fragments/autokosten")
    public String getAutokosten(Model model) {
        fillAutokosten(model);
        return "fragments/autokosten";
    }

    public void fillAutokosten(Model model) {
        List<Autokosten> entries = repository.findAllByOrderByGesamtkilometerDesc();
        setAverage(entries);
        model.addAttribute("entries", entries);
        model.addAttribute("stage", stage);
        model.addAttribute("gesamt", getGesamt(entries));
    }

    @PostMapping(value = "/fragments/autokosten")
    public String postAutokosten(@ModelAttribute Autokosten autokosten, Model model) {
        repository.save(autokosten);
        List<Autokosten> entries = repository.findAllByOrderByGesamtkilometerDesc();
        setAverage(entries);
        model.addAttribute("entries", entries);
        model.addAttribute("stage", stage);
        model.addAttribute("gesamt", getGesamt(entries));
        return "fragments/autokosten";
    }

    private void setAverage(List<Autokosten> entries) {
        long lastGesamtKilometer = 0;
        for (int i = entries.size() - 1; i >= 0; i--) {
            Autokosten entry = entries.get(i);
            long gesamtKilometer = entry.getGesamtkilometer();
            if (lastGesamtKilometer > 0) {
                BigDecimal liter = entry.getLiter();
                long tageskilometer = gesamtKilometer - lastGesamtKilometer;
                if (tageskilometer > 0) {
                    entry.setAverage(liter.multiply(BigDecimal.valueOf(100)).divide(BigDecimal.valueOf(tageskilometer), 2, RoundingMode.HALF_DOWN));
//                    if (entry.getAverage().compareTo(BigDecimal.valueOf(10)) > 0) {
//                        System.out.println(entries);
//                    }
                }
            }
            lastGesamtKilometer = gesamtKilometer;
        }
    }

    private Map<String, Object> getGesamt(List<Autokosten> entries) {
        Map<String, Object> map = new HashMap<>();

        Autokosten gesamt = new Autokosten();
        for (Autokosten entry : entries) {
            gesamt.setBetrag(gesamt.getBetrag().add(entry.getBetrag()));
            gesamt.setTageskilometer(gesamt.getTageskilometer() + entry.getTageskilometer());
            gesamt.setLiter(gesamt.getLiter().add(entry.getLiter()));
        }

        long gesamtkilometer = 0;
        BigDecimal average = BigDecimal.ZERO;
        if (!entries.isEmpty()) {
            gesamtkilometer = entries.get(0).getGesamtkilometer() - entries.get(entries.size() - 1).getGesamtkilometer();
            average =  gesamt.getLiter().multiply(BigDecimal.valueOf(100)).divide(BigDecimal.valueOf(gesamtkilometer), 2, RoundingMode.HALF_DOWN);
        }
        map.put("betrag", gesamt.getBetrag());
        map.put("tageskilometer", gesamt.getTageskilometer());
        map.put("gesamtkilometer", gesamtkilometer);
        map.put("liter", gesamt.getLiter());
        map.put("average", average);
        return map;
    }
}
