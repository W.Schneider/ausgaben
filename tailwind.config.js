/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/main/resources/static/**/*.{html,js}", "./src/main/resources/templates/**/*.{html,js}"],
  theme: {
    extend: {
      backgroundImage: {
        'arrow' : "url('/img/arrow.svg')",
      }
    },
  },
  plugins: [],
}

