DROP TABLE IF EXISTS AUSGABE;

CREATE TABLE AUSGABE
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    datum       DATE           NOT NULL,
    bezeichnung VARCHAR(250)   NOT NULL,
    betrag      DECIMAL(6, 2) NOT NULL,
    kategorie   VARCHAR(250)   NOT NULL
);